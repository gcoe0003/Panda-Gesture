﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorUser : MonoBehaviour
{
    public Vector3 rotAxis;
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(rotAxis*Time.deltaTime);
    }
}
