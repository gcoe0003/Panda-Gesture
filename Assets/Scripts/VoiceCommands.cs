﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using RosSharp.RosBridgeClient;
using RosSharp;
using Microsoft.MixedReality.Toolkit.UI;
using System;
using TMPro;
using RosSharp.RosBridgeClient.MessageTypes.Moveit;

/* Written by Steven Lay and Steven Hoang, 2020.
 * Methods for the voice commands are stored here.
 * Based off the original RobotSpeechManager modified by Caris Lab students.
 * Remove all Caris Lab student code unused by us.
 * Modified on 19/07/2020 by Steven Hoang with the new ROS-sharp integration
 */
public class VoiceCommands : MonoBehaviour
{
    // Make empty object that is location of primary pointer
    private GameObject pointerPos;

    // Set execution mode suitable for Steven's user study
    [SerializeField]
    private bool stevensExperiment = false;

    // Prefab for Set Point ball
    [SerializeField]
    private Transform spherePoint = null;

    // Prefab for Cube Barrier
    [SerializeField]
    private Transform cubeBarrierPrefab = null;

    // Prefab for Sphere Barrier
    [SerializeField]
    private Transform sphereBarrierPrefab = null;

    // Virtual person barrier
    [SerializeField]
    private BarrierPerson barrierPerson = null;

    // Create a line for use in displaying planned trajectory from MoveIt
    private LineRenderer lineConnecting;

    // ROS Connector to communicate with ROS
    private GameObject ROSConnector;
    // Robot Transform
    private GameObject RobotModel;
    private Transform ros_world_coord_frame;

    private List<Vector3> pathPoints = new List<Vector3>();

    public static int barrierCount { get; set; } = 0;

    public Vector3 EEF_Offset;

    private Boolean usingGaze = true;

    public bool acceptingInstructions;

    public bool inMotion;

    public int runType;

    public SceneWatcher sceneWatcher;

    public PointPlacer pointPlacer;

    [SerializeField]
    public TextMeshPro commandText;

    public Vector3 toMePoint = Vector3.zero;

    void Start()
    {
        // Find ROS Connector to communicate with ROS side
        ROSConnector = GameObject.Find("ROS Connector");
        // Find the RobotModel and obtain its "world" coordinate frame
        RobotModel = GameObject.FindGameObjectWithTag("Robot");
        ros_world_coord_frame = RobotModel.transform.Find("world").transform;

        pointerPos = new GameObject("pointerPos");
        CoreServices.InputSystem?.FocusProvider?.SubscribeToPrimaryPointerChanged(OnPrimaryPointerChanged, true);

        // Set the default tag for all spawned setPoint prefab instances for easy removal later on.
        spherePoint.tag = "clone";

        acceptingInstructions = true;
        runType = 0;
    }

    // From Primary Pointer MRTK example, we need this to track the Primary pointer and place the pointerPos on it
    private void OnPrimaryPointerChanged(IMixedRealityPointer oldPointer, IMixedRealityPointer newPointer)
    {
        if (pointerPos != null)
        {
            if (newPointer != null)
            {
                Transform parentTransform = newPointer.BaseCursor?.GameObjectReference?.transform;

                // If there's no cursor try using the controller pointer transform instead
                if (parentTransform == null)
                {
                    var controllerPointer = newPointer as BaseControllerPointer;
                    parentTransform = controllerPointer?.transform;
                }

                if (parentTransform != null)
                {
                    pointerPos.transform.SetParent(parentTransform, false);
                    pointerPos.SetActive(true);
                    pointerPos.transform.position = parentTransform.position;
                    return;
                }
            }

            pointerPos.SetActive(false);
            pointerPos.transform.SetParent(null, false);
        }
    }

    // If the Robot is misaligned wrt the Image Marker, attempts to one-shot recalibrate it
    public void Recalibrate()
    {
        GameObject.Find("ImageTarget").GetComponent<CustomDefaultTrackableEventHandler>().recalibrate = true;
        GameObject.Find("ImageTarget").GetComponent<CustomDefaultTrackableEventHandler>().reActivate();
    }

    // Destroys all points and barriers visible
    public void ClearAll()
    {
        ClearRoutine(true, true);
        if (commandText != null) commandText.text = "Clear All";
    }

    public void ClearBarriers()
    {
        ClearRoutine(false, true);
        if (commandText != null) commandText.text = "Clear All Barriers";
    }

    public void ClearPoints()
    {
        ClearRoutine(true, false);
        if (commandText!=null) commandText.text = "Clear All Points";
    }

    private void ClearRoutine(bool clearPoints, bool clearBarriers)
    {
        if (clearPoints && !pointPlacer.loading)
        {
            // Clearing the Additional points formed
            var pointPlacer = GameObject.Find("Points").transform;
            pointPlacer.GetComponent<PointPlacer>().ToPoint();
            foreach (Transform obj in pointPlacer)
            {
                Destroy(obj.gameObject);
            }
            pointPlacer.GetComponent<PointPlacer>().allPoints.Clear();

            pathPoints.Clear();
            var clones = GameObject.FindGameObjectsWithTag("clone");
            foreach (var clone in clones)
            {
                Destroy(clone);
            }

            pointPlacer.GetComponent<PointPlacer>().miniMenu.SetActive(false);
            pointPlacer.GetComponent<PointPlacer>().activeMenu = null;
            pointPlacer.GetComponent<PointPlacer>().miniMenuHit = false;

            if (pointPlacer.GetComponent<PointPlacer>().selectedObjs.Count == 1)
            {
                runType = 2;
                if (pointPlacer.GetComponent<PointPlacer>().placeFlag) runType = 3;
            }
            else runType = 0;
            

            // Clear the displayed trajectory line
            // ROSConnector.GetComponent<TrajectoryLineDisplay>().ClearTrajectoryLine();
        }

        if (clearBarriers)
        {
            List<string> barrierNames = new List<string>();
            var bars = GameObject.FindGameObjectsWithTag("Barrier");
            int numObj = 0;
            foreach (var bar in bars)
            {
                if (!bar.name.Contains("Object")) Destroy(bar);
                else numObj++;
            }
            barrierCount = numObj;
        }
    }

    public void AutoLockPath()
    { 
        LockPath();
    }

    // Sends goal points to MoveIt for a trajectory to be planned
    public void LockPath(float rotx = 0.923956f, float roty = -0.382499f, float rotz = 0f, float rotw = 0f)
    {
        if (pathPoints != null)
        {
            List<RosSharp.RosBridgeClient.MessageTypes.Geometry.Pose> goal_points = new List<RosSharp.RosBridgeClient.MessageTypes.Geometry.Pose>();
            // Hardcoded orientation of the EEF, gonna look for a better way to configure this
            RosSharp.RosBridgeClient.MessageTypes.Geometry.Quaternion quaternion = new RosSharp.RosBridgeClient.MessageTypes.Geometry.Quaternion
                                                                                                                ((double)rotx, (double)roty, (double)rotz, (double)rotw);
            // Construct array of goalpoints
            foreach (var point in pathPoints)
            {
                RosSharp.RosBridgeClient.MessageTypes.Geometry.Point position = GetGeometryPoint(point.Unity2Ros());
                Debug.Log(point);
                goal_points.Add(new RosSharp.RosBridgeClient.MessageTypes.Geometry.Pose(position, quaternion));
            }
            var pointsObjects = GameObject.FindGameObjectsWithTag("clone");
            foreach (var point in pointsObjects)
            {
                point.GetComponent<MeshRenderer>().material.color = Color.blue;
            }
            Debug.Log(goal_points[0].ToString());
            ROSConnector.GetComponent<PathRequest>().SendRequest(goal_points);
            acceptingInstructions = true;
        }
    }

    // Causes Robot to begin following trajectory path planned
    public void Execute()
    {

        if (stevensExperiment)
        {
            // Do experimental procedure here
        }
        else if (acceptingInstructions)
        {
            ROSConnector.GetComponent<RobotCommandPublisher>().SendCommand(RobotCommandPublisher.EXECUTE_TRIGGER);
            acceptingInstructions = false;
            inMotion = true;
            StartCoroutine(WaitForFinish());
        }
    }

    public IEnumerator WaitForFinish()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            if (RosSharp.RosBridgeClient.UnityStateSubscriber.robot_state == 1) break;
        }
        inMotion = false;
    }

    public void RobotRun()
    {
        float rotx = 0.923956f;
        float roty = -0.382499f;
        float rotz = 0f;
        float rotw = 0f;

        StartCoroutine(RunProcedure(rotx, roty, rotz, rotw));
    }

    public IEnumerator RunProcedure(float rotx, float roty, float rotz, float rotw)
    {
        pointPlacer.MakeLoop();
        yield return new WaitForSeconds(0.5f);

        switch (runType)
        {
            case 1:
                if (commandText != null) commandText.text = "Run Path";
                LockPath(rotx, roty, rotz, rotw);
                yield return new WaitForSeconds(1f);
                Execute();
                break;
            case 2:
                if (commandText != null) commandText.text = "Run Pick";
                sceneWatcher.PickUpdate();
                runType = (pathPoints.Count == 0) ? 0 : 1;
                inMotion = true;
                break;
            case 3:
                if (commandText != null) commandText.text = "Run Place";
                if (toMePoint != Vector3.zero)
                {
                    pointPlacer.allPoints.Add(toMePoint);
                    toMePoint = Vector3.zero;
                }
                pointPlacer.Place();
                runType = (pathPoints.Count != 0) ? 0 : 1;
                inMotion = true;
                break;
            default:
                break;
        }
    }
    public void Pick()
    {
        if (commandText != null) commandText.text = "Pick";
        runType = 2;
    }
    public void Place()
    {
        pointPlacer.MakeLoop();
        if (commandText != null) commandText.text = "Place";
        Destroy(GameObject.FindGameObjectWithTag("Ghost"));
        runType = 3;
        if (pointPlacer.placeFlag)
        {
            Vector3 placePoint = toMePoint;
            Quaternion ghostOrientation = transform.rotation;

            Debug.Log(toMePoint);
            GameObject ghostObject;

            if (toMePoint == Vector3.zero)
            {
                placePoint = pointPlacer.allPoints[pointPlacer.allPoints.Count - 1];
                ghostObject = Instantiate(sceneWatcher.pickedObj, placePoint + sceneWatcher.pickedObj.transform.localScale.y*0.5f*Vector3.up, ghostOrientation);
            }
            else ghostObject = Instantiate(sceneWatcher.pickedObj, placePoint, ghostOrientation);

            ghostObject.tag = "Ghost";
            ghostObject.name = "Place Ghost";
            ghostObject.GetComponent<MeshRenderer>().material.color = Color.blue;
        }
    }

    public void ClearBarriersLoop()
    {
        pointPlacer.ClearLoop(true);
    }
    public void ClearPointsLoop()
    {
        pointPlacer.ClearLoop(false);
    }

    public void ToUser()
    {
        StartCoroutine(ToUserAsync());
    }
    public void ResetDraw()
    {
        ClearPoints();
        pointPlacer.PointerUp();
        pointPlacer.pointerUp = true;
        StartCoroutine(pointPlacer.WaitForUp(0.2f));

    }

    public IEnumerator ToUserAsync()
    {
        if (commandText != null) commandText.text = "To Me";
        Vector3 toRobot = RobotModel.transform.position - Camera.main.transform.position;
        List<RosSharp.RosBridgeClient.MessageTypes.Geometry.Pose> goal_points = new List<RosSharp.RosBridgeClient.MessageTypes.Geometry.Pose>();

        toMePoint = RobotModel.transform.position - toRobot.normalized * 0.75f + EEF_Offset;

        Debug.Log(toMePoint);

        RosSharp.RosBridgeClient.MessageTypes.Geometry.Point position = GetGeometryPoint((ros_world_coord_frame.InverseTransformPoint(toMePoint)).Unity2Ros());
        goal_points.Add(new RosSharp.RosBridgeClient.MessageTypes.Geometry.Pose(position, GetGeometryQuaternion(new Quaternion(0.923956f, -0.382499f, 0f, 0f))));

        ROSConnector.GetComponent<PathRequest>().SendRequest(goal_points);
        acceptingInstructions = true;
        yield return new WaitForSeconds(1f);
        Execute();
    }

    // Requests for the Robot to stop
    public void Terminate()
    {
        // barrierPerson.Hide = true;
        ROSConnector.GetComponent<RobotCommandPublisher>().SendCommand(RobotCommandPublisher.STOP_TRIGGER);
    }

    // Requests for the Robot to move to Ready State
    public void ReadyState()
    {
        if (commandText != null) commandText.text = "Ready State";
        // barrierPerson.Hide = true;
        
        //ROSConnector.GetComponent<RobotCommandPublisher>().SendCommand(RobotCommandPublisher.READY_STATE_TRIGGER);
        
        StartCoroutine(ToReadyAsync());
    }

    public IEnumerator ToReadyAsync()
    {
        Vector3 readyStatePt = new Vector3(0.4f, 0f, 0.8f);

        List<RosSharp.RosBridgeClient.MessageTypes.Geometry.Pose> goal_points = new List<RosSharp.RosBridgeClient.MessageTypes.Geometry.Pose>();
        RosSharp.RosBridgeClient.MessageTypes.Geometry.Point position = GetGeometryPoint(readyStatePt);
        goal_points.Add(new RosSharp.RosBridgeClient.MessageTypes.Geometry.Pose(position, GetGeometryQuaternion(new Quaternion(0.923956f, -0.382499f, 0f, 0f))));

        ROSConnector.GetComponent<PathRequest>().SendRequest(goal_points);
        acceptingInstructions = true;
        yield return new WaitForSeconds(1f);
        Execute();
    }

    // Set the Robot mode to preview control
    public void Preview()
    {
        // ROSConnector.GetComponent<TrajectoryActionDisplay>().PreviewStart();
    }

    // Sets a goal point at the current gaze position, must be on workspace
    public void SetPoint()
    {
        Transform child;
        if (commandText != null) commandText.text = "Set Point";

        if (usingGaze)
        {
            GameObject focusedObject = CoreServices.InputSystem.GazeProvider.GazeTarget;
            if (focusedObject == null || focusedObject.layer != LayerMask.NameToLayer("Drawable"))
            {
                Debug.Log("Barrier not set on workspace, backing out");
                return;
            }

            child = Instantiate(spherePoint, CoreServices.InputSystem.GazeProvider.HitPosition, Quaternion.identity);
        }
        else
        {
            GameObject focusedObject = CoreServices.InputSystem?.FocusProvider?.GetFocusedObject(CoreServices.InputSystem?.FocusProvider?.PrimaryPointer);
            if (focusedObject == null || focusedObject.layer != LayerMask.NameToLayer("Drawable"))
            {
                Debug.Log("Barrier not set on workspace, backing out");
                return;
            }

            child = Instantiate(spherePoint, pointerPos.transform.position, Quaternion.identity);
        }

        pathPoints.Add(ros_world_coord_frame.InverseTransformPoint(child.position) + EEF_Offset);

        Debug.LogFormat("Adding point {0} at {1}", pathPoints.Count, ros_world_coord_frame.InverseTransformPoint(child.position).ToString("F3"));
    }

    public void PickupObjectGaze()
    {
        if (commandText != null) commandText.text = "Pick";

        GameObject focusedObject;

        if (usingGaze)
        {
            focusedObject = CoreServices.InputSystem.GazeProvider.GazeTarget;
            if (focusedObject == null || focusedObject.layer != LayerMask.NameToLayer("Selectable"))
            {
                return;
            }
        }
        else
        {
            focusedObject = CoreServices.InputSystem?.FocusProvider?.GetFocusedObject(CoreServices.InputSystem?.FocusProvider?.PrimaryPointer);
            if (focusedObject == null || focusedObject.layer != LayerMask.NameToLayer("Selectable"))
            {
                return;
            }
        }

        pointPlacer.UnselectAll();
        pointPlacer.selectedObjs.Add(focusedObject);
        Pick();
    }

    public void AddToPath(Vector3 point)
    {
        Instantiate(spherePoint, point, Quaternion.identity);

        if (ros_world_coord_frame != null) pathPoints.Add(ros_world_coord_frame.InverseTransformPoint(point) + EEF_Offset);

        if (runType != 2) runType = 1;
        Debug.Log(runType);

        // Debug.LogFormat("Adding point {0} at {1}", pathPoints.Count, ros_world_coord_frame.InverseTransformPoint(point).ToString("F3"));
    }

    public void ClearLastPoint()
    {
        var clones = GameObject.FindGameObjectsWithTag("clone");

        if (clones.Length > 0)
        {
            Destroy(clones[clones.Length - 1]);
        }
    }

    public void AddCubeBarrier()
    {
        AddBarrier(0);
    }

    public void AddSphereBarrier()
    {
        AddBarrier(1);
    }

    public void HandRaysOff()
    {
        PointerUtils.SetHandRayPointerBehavior(PointerBehavior.AlwaysOff);
        PointerUtils.SetGazePointerBehavior(PointerBehavior.AlwaysOn);

        usingGaze = true;
    }

    public void HandRaysOn()
    {
        PointerUtils.SetHandRayPointerBehavior(PointerBehavior.Default);
        PointerUtils.SetGazePointerBehavior(PointerBehavior.AlwaysOff);

        usingGaze = false;
    }

    /* Adds a Cube or Sphere primitive barrier at the current gaze position, must be on workspace
     * int type: 0 = Cube, 1 = Sphere
     */
    private void AddBarrier(int type)
    {
        Transform child;
        Transform barrierType = (type == 0) ? cubeBarrierPrefab : sphereBarrierPrefab;

        if (usingGaze)
        {
            GameObject focusedObject = CoreServices.InputSystem.GazeProvider.GazeTarget;
            if (focusedObject == null || focusedObject.tag != "Workspace")
            {
                Debug.Log("Barrier not set on workspace, backing out");
                return;
            }

            child = Instantiate(barrierType, CoreServices.InputSystem.GazeProvider.HitPosition, Quaternion.identity);
        }
        else
        {
            GameObject focusedObject = CoreServices.InputSystem?.FocusProvider?.GetFocusedObject(CoreServices.InputSystem?.FocusProvider?.PrimaryPointer);
            if (focusedObject == null || focusedObject.tag != "Workspace")
            {
                Debug.Log("Barrier not set on workspace, backing out");
                return;
            }

            child = Instantiate(barrierType, pointerPos.transform.position, Quaternion.identity);
        }

        child.tag = "Barrier";
        child.name = "Barrier" + barrierCount++;
        child.transform.parent = null;
        child.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        child.transform.parent = ros_world_coord_frame;

        Debug.Log("Adding barrier");
    }

    public Transform AddBarrierAtPoint(int type, Vector3 pt, Quaternion rot, Vector3 scl)
    {
        Transform child;
        Transform barrierType = (type == 0) ? cubeBarrierPrefab : sphereBarrierPrefab;

        child = Instantiate(barrierType, pt, rot);

        child.tag = "Barrier";
        child.name = "Barrier" + barrierCount++;
        child.transform.localScale = scl;
        child.GetComponent<AudioSource>().enabled = false;
        child.GetComponent<BoundingBox>().enabled = false;
        child.GetComponent<NearInteractionGrabbable>().enabled = false;
        child.GetComponent<ObjectManipulator>().enabled = false;

        return child;
    }
        

        // Remove barrier currently gazed at
    public void RemoveBarrier()
    {
        GameObject GazedObj = CoreServices.InputSystem.GazeProvider.GazeTarget;

        if (GazedObj.CompareTag("Barrier"))
        {
            Destroy(GazedObj);
            barrierCount--;
        }
    }

    // Remove barrier currently gazed at
    public void RemoveThisBarrier(GameObject barrier)
    {
        if (!barrier.name.Contains("Object")) Destroy(barrier);
        barrierCount--;
    }

    public RosSharp.RosBridgeClient.MessageTypes.Geometry.Point GetGeometryPoint(Vector3 position)
    {
        RosSharp.RosBridgeClient.MessageTypes.Geometry.Point geometryPoint = new RosSharp.RosBridgeClient.MessageTypes.Geometry.Point();
        geometryPoint.x = position.x;
        geometryPoint.y = position.y;
        geometryPoint.z = position.z;
        return geometryPoint;
    }
    public RosSharp.RosBridgeClient.MessageTypes.Geometry.Quaternion GetGeometryQuaternion(Quaternion quaternion)
    {
        RosSharp.RosBridgeClient.MessageTypes.Geometry.Quaternion geometryQuaternion = new RosSharp.RosBridgeClient.MessageTypes.Geometry.Quaternion();
        geometryQuaternion.x = quaternion.x;
        geometryQuaternion.y = quaternion.y;
        geometryQuaternion.z = quaternion.z;
        geometryQuaternion.w = quaternion.w;
        return geometryQuaternion;
    }
}
