﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineCollider : MonoBehaviour
{
    private void OnTriggerEnter(Collider col)
    {
        transform.parent.parent.GetComponent<PointPlacer>().FinishLoop(col, this.transform.gameObject);
    }
}
