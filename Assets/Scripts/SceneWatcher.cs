﻿using Microsoft.MixedReality.Toolkit;
using RosSharp;
using RosSharp.RosBridgeClient.MessageTypes.Moveit;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions.Must;

/* Written by Steven Hoang 2020
 * The SceneWatcher acts as a periodic publisher, which advertises the position and dimensions of all the barriers presented in the scene. 
 * This publisher advertises this information on rosbridge and it would be captured by the planner running on the ROS side.
 * Modified on 18/07/2020 by Steven Hoang with the new ROS-sharp integration
 */
namespace RosSharp.RosBridgeClient
{
	public class SceneWatcher : MonoBehaviour
	{
		// Rate at which collision objects are published to ROS
		[SerializeField]
		public float ROSUpdateRate = 0.5f;
		public PointPlacer PointPlacer;
		public GameObject pickedObj;

		public VoiceCommands VoiceCommands;
		public RobotStatusUpdate RobotStatus;
		public Quaternion pickOrientation;

		private bool pickFlag;
		private float pickHeight;

		bool pauseScene;

		private GameObject ROSConnector;

		private GameObject[] barriers;
		
		private GameObject personBarrier;
		private MessageTypes.Moveit.CollisionObject pb_message;
		// private GameObject table;
		private MessageTypes.Moveit.CollisionObject workspace1;
		private MessageTypes.Moveit.CollisionObject workspace2;
		private MessageTypes.Moveit.CollisionObject workspace3;
		private MessageTypes.Moveit.CollisionObject workspace4;

		private GameObject RobotModel;
		private Transform ros_world_coord_frame;

		private MessageTypes.Moveit.PlanningSceneWorld planning_scene;
		private List<MessageTypes.Moveit.CollisionObject> barrier_msgs;

		void Start()
		{
			pickOrientation = new Quaternion(0.923956f, -0.382499f, 0f, 0f);
			pickOrientation *= Quaternion.Euler(0, 0, 90);

			pickHeight = 0.1f;

			pickFlag = false;

			pauseScene = false;

			personBarrier = GameObject.FindWithTag("PersonBarrier");
			// table = GameObject.Find("Table");
			// Connect with ROS
			ROSConnector = GameObject.Find("ROS Connector");

			// Find the Coordinate System of ROS
			RobotModel = GameObject.FindGameObjectWithTag("Robot");
			ros_world_coord_frame = RobotModel.transform.Find("world");
			//// Initialise ROS messages
			planning_scene = new MessageTypes.Moveit.PlanningSceneWorld();
			#region Generic Barrier
			barrier_msgs = new List<MessageTypes.Moveit.CollisionObject>();
            #endregion
            #region Person Barrier
            pb_message = new MessageTypes.Moveit.CollisionObject();
			pb_message.header.frame_id = "world";
			pb_message.id = "Person";
			pb_message.primitives = new MessageTypes.Shape.SolidPrimitive[] // CYLINDER needs height and radius
							{new MessageTypes.Shape.SolidPrimitive(MessageTypes.Shape.SolidPrimitive.CYLINDER, new double[2]) };
			pb_message.primitive_poses = new MessageTypes.Geometry.Pose[] { new MessageTypes.Geometry.Pose() };
			pb_message.primitive_poses[0].orientation = new MessageTypes.Geometry.Quaternion(0, 0, 0, 1);
			#endregion
			#region Workspace
			workspace1 = new MessageTypes.Moveit.CollisionObject();
			workspace1.header.frame_id = "world";
			workspace1.id = "Workspace1";
			workspace1.primitives = new MessageTypes.Shape.SolidPrimitive[] // Only one mesh, with (x, y, z) dimensions listed below
				{new MessageTypes.Shape.SolidPrimitive(MessageTypes.Shape.SolidPrimitive.BOX, new double[3] { 1.80f, 1.80f, 0.02f }) };
			workspace1.primitive_poses = new MessageTypes.Geometry.Pose[] { new MessageTypes.Geometry.Pose() };
			workspace1.primitive_poses[0].position = new MessageTypes.Geometry.Point(0, 0, -0.02);
			workspace1.primitive_poses[0].orientation = new MessageTypes.Geometry.Quaternion(0, 0, 0, 1);
			#endregion


			// Start updating the planningScene to ROS
			StartCoroutine(RosBarrierUpdate());
		}

		void LateUpdate()
		{
			//Debug.Log(personBarrier.transform.position);
		}

		IEnumerator RosBarrierUpdate()
		{
			while (true)
			{
				// Reset the barrier array for a new PlanningSceneWorld message
				barrier_msgs.Clear();
				// Always add the workspace1 as a barrier/collision object
				// Debug.Log(ros_world_coord_frame.InverseTransformPoint(table.transform.position).Unity2Ros());
				// barrier_msgs.Add(workspace1);
				//barrier_msgs.Add(workspace2);
				//barrier_msgs.Add(workspace3);
				//barrier_msgs.Add(workspace4);
				// Person Barrier
				// If the barrier is activated
				if (!personBarrier.GetComponent<BarrierPerson>().Hide)
				{
					// Update the barrier message and added it to the barrier list
					// Dimensions
					pb_message.primitives[0].dimensions[MessageTypes.Shape.SolidPrimitive.CYLINDER_HEIGHT] = 1.75f; //TODO: The scale is hardcoded due to the mismatch in Unity Scale with the actual size
					pb_message.primitives[0].dimensions[MessageTypes.Shape.SolidPrimitive.CYLINDER_RADIUS] = personBarrier.transform.localScale.x/2;
					// Pose
					pb_message.primitive_poses[0].position = GetGeometryPoint(ros_world_coord_frame.InverseTransformPoint(personBarrier.transform.position).Unity2Ros());
					barrier_msgs.Add(pb_message);
				}
                // Other barriers
                barriers = GameObject.FindGameObjectsWithTag("Barrier");
				MessageTypes.Moveit.CollisionObject pickMsgObj = new MessageTypes.Moveit.CollisionObject();
				bool goToPick = false;

				foreach (GameObject barrier in barriers)
				{
					if (pickFlag && barrier == PointPlacer.selectedObjs[0])
					{
						AddBarrier(barrier);
						pickedObj = barrier;
						pickMsgObj = barrier_msgs[barrier_msgs.Count - 1];
						goToPick = true;
					}
					else AddBarrier(barrier);
				}
				// Update the message and publish to ROS
				planning_scene.collision_objects = barrier_msgs.ToArray();
				if (!pauseScene) ROSConnector.GetComponent<CollisionObjectManager>().UpdateScene(planning_scene);

				yield return new WaitForSeconds(ROSUpdateRate);

				if(pickMsgObj != null && goToPick)
                {
					PointPlacer.pickBounds = Instantiate(pickedObj, ros_world_coord_frame.position - Vector3.up * 10.0f, Quaternion.identity); ;
					PointPlacer.pickBounds.transform.localScale *= 1.0f;
					PointPlacer.pickBounds.tag = "Checker";
					Destroy(PointPlacer.pickBounds.GetComponent<BarrierCollider>());
					// Destroy(PointPlacer.pickBounds.GetComponent<MeshRenderer>());
					PointPlacer.pickBounds.SetActive(true);
					StartCoroutine(PickUpRoutine(pickedObj, pickMsgObj));
					pickFlag = false;
				}

			}
		}
		public void PickUpdate()
		{
			pickFlag = (PointPlacer.selectedObjs.Count == 1) && !VoiceCommands.inMotion;
		}

		public IEnumerator PickUpRoutine(GameObject barrier, MessageTypes.Moveit.CollisionObject collisionObject)
		{

			List<MessageTypes.Geometry.Pose> goal_points = new List<MessageTypes.Geometry.Pose>();

			pickOrientation = new Quaternion(0.923956f, -0.382499f, 0f, 0f);

			pickOrientation *= Quaternion.Euler(0, 0, (90-Vector3.SignedAngle(barrier.transform.forward, ros_world_coord_frame.forward, ros_world_coord_frame.up)));


			pickHeight = barrier.transform.localScale.y;

			Vector3 tempPoint = (ros_world_coord_frame.InverseTransformPoint(barrier.transform.position + new Vector3(0, pickHeight / 2, 0))) + VoiceCommands.EEF_Offset;

			MessageTypes.Geometry.Point position = GetGeometryPoint((tempPoint).Unity2Ros());
			goal_points.Add(new MessageTypes.Geometry.Pose(position, GetGeometryQuaternion(pickOrientation)));
			Debug.Log(position);
			ROSConnector.GetComponent<PathRequest>().SendRequest(goal_points);
			VoiceCommands.acceptingInstructions = true;
			yield return new WaitForSeconds(1f);
			VoiceCommands.Execute();
			pauseScene = true;

			while (true)
			{
				yield return new WaitForSeconds(1f);
				if (RosBridgeClient.UnityStateSubscriber.robot_state == 1) break;
			}
			pickedObj.tag = "Picked";
			ROSConnector.GetComponent<PickRequest>().UpdateScene(collisionObject);
			PointPlacer.placeFlag = true;
			while (true)
			{
				yield return new WaitForSeconds(1f);
				if (RosBridgeClient.UnityStateSubscriber.robot_state == 1) break;
			}
			pauseScene = false;
			pickOrientation = new Quaternion(0.923956f, -0.382499f, 0f, 0f);
		}

		public IEnumerator PlaceRoutine(Vector3 placePoint)
		{
			List<MessageTypes.Geometry.Pose> goal_points = new List<MessageTypes.Geometry.Pose>();
			MessageTypes.Geometry.Point position = GetGeometryPoint((ros_world_coord_frame.InverseTransformPoint(placePoint) + VoiceCommands.EEF_Offset).Unity2Ros());
			goal_points.Add(new MessageTypes.Geometry.Pose(position, GetGeometryQuaternion(pickOrientation)));

			ROSConnector.GetComponent<PathRequest>().SendRequest(goal_points);
			Debug.Log(goal_points[0].ToString());
			VoiceCommands.acceptingInstructions = true;
			yield return new WaitForSeconds(1f);
			VoiceCommands.Execute();

			while (true)
			{
				yield return new WaitForSeconds(1f);
				if (RosBridgeClient.UnityStateSubscriber.robot_state == 1) break;
			}

			MessageTypes.Geometry.Vector3 point_msg = new MessageTypes.Geometry.Vector3();
			Vector3 temp = ros_world_coord_frame.InverseTransformPoint(placePoint).Unity2Ros();

			point_msg.x = temp.x;
			point_msg.y = temp.y;
			point_msg.z = Mathf.Abs(temp.z);

			ROSConnector.GetComponent<PlaceRequest>().UpdateScene(point_msg);
			Debug.Log("gripper");
			while (true)
			{
				yield return new WaitForSeconds(1f);
				if (RosBridgeClient.UnityStateSubscriber.robot_state == 1) break;
			}
			if (pickedObj != null)
			{
				pickedObj.tag = "Barrier";
				PointPlacer.pickBounds = null;
				Destroy(GameObject.FindGameObjectWithTag("Ghost"));
				pickedObj.GetComponent<OneShotVuforia>().Refresh();
				PointPlacer.UnselectAll();
				pickedObj = null;
			}
		}

		public void AddBarrier(GameObject barrier)
		{
			MessageTypes.Moveit.CollisionObject barrier_msg = new MessageTypes.Moveit.CollisionObject();
			barrier_msg = new MessageTypes.Moveit.CollisionObject();
			barrier_msg.header.frame_id = "world";
			barrier_msg.primitive_poses = new MessageTypes.Geometry.Pose[] { new MessageTypes.Geometry.Pose() };
			barrier_msg.id = barrier.name;
			barrier_msg.primitive_poses[0].position = GetGeometryPoint(ros_world_coord_frame.InverseTransformPoint(barrier.transform.position).Unity2Ros());
			Vector3 tempForward = barrier.transform.forward;

			barrier_msg.primitive_poses[0].orientation = GetGeometryQuaternion(Quaternion.FromToRotation(ros_world_coord_frame.forward,tempForward).Unity2Ros());

			// Check the type of barrier
			if (barrier.GetComponent<MeshFilter>().sharedMesh.name == "Sphere")
			{
				// Dimension required is only the radius of the sphere
				barrier_msg.primitives = new MessageTypes.Shape.SolidPrimitive[1]
					{new MessageTypes.Shape.SolidPrimitive(MessageTypes.Shape.SolidPrimitive.SPHERE,
													new double[]{ barrier.transform.localScale.z / 2.0f })}; // Calculate the radius
				barrier_msgs.Add(barrier_msg);
			}
			else
			{
				// Dimensions required are the 3 size of the cube
				barrier_msg.primitives = new MessageTypes.Shape.SolidPrimitive[]
					{new MessageTypes.Shape.SolidPrimitive(MessageTypes.Shape.SolidPrimitive.BOX,
													 barrier.transform.localScale.Unity2RosScale().ToRoundedDoubleArray())};
				barrier_msgs.Add(barrier_msg);
				//Debug.Log(barrier_msg);
			}
		}

		public MessageTypes.Geometry.Point GetGeometryPoint(Vector3 position)
		{
			MessageTypes.Geometry.Point geometryPoint = new MessageTypes.Geometry.Point();
			geometryPoint.x = position.x;
			geometryPoint.y = position.y;
			geometryPoint.z = position.z;
			return geometryPoint;
		}
		public MessageTypes.Geometry.Quaternion GetGeometryQuaternion(Quaternion quaternion)
		{
			MessageTypes.Geometry.Quaternion geometryQuaternion = new MessageTypes.Geometry.Quaternion();
			geometryQuaternion.x = quaternion.x;
			geometryQuaternion.y = quaternion.y;
			geometryQuaternion.z = quaternion.z;
			geometryQuaternion.w = quaternion.w;
			return geometryQuaternion;
		}
	}
}