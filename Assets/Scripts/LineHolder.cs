﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineHolder : MonoBehaviour
{
    public GameObject markerFab;
    public GameObject planeMeshFab;
    public GameObject spaceFillingCurveLine;
    public List<Vector3> perimPoints;
    public List<Vector3> planePoints;
    public List<Vector2> planePointsXY;
    public List<Vector3> spaceFillingCurve;
    public List<Vector3> spaceFillingCurveXY;
    public List<Vector3> spaceFillingCurvePlane;
    public float fillingCurveLimitx;
    public float fillingCurvex;
    public float fillingCurvey;
    public float planeAngleThreshold;
    public float pointDeltaThreshod;

    public float meshLen;
    public List<int> liftPointIndex;
    public LayerMask drawMask;
    public Vector3 meanPoint;
    public Vector3 normVec;

    private Vector3 closePoint;
    private Vector3 farPoint;
    private GameObject planeMesh;
    private float PlaneZ;
    private float minX;
    private float minY;
    private float maxX;
    private float maxY;

    public void SetPoints(List<Vector3> points)
    {
        perimPoints = new List<Vector3>(points);
        planePoints = new List<Vector3>(points);

        meanPoint = Vector3.zero;
        for (int i = 0; i < perimPoints.Count; i++)
        {
            meanPoint += perimPoints[i];
            planePointsXY.Add(points[i]);
        }
        meanPoint /= perimPoints.Count;
        CloseFarPointUpdate();
    }
    public void GenPlane()
    {
        CloseFarPointUpdate();
        normVec = Camera.main.transform.position - closePoint;

        for (int i = 0; i < perimPoints.Count; i++)
        {
            planePoints[i] = WorldToPlaneXY(perimPoints[i]);
            planePointsXY[i] = Quaternion.FromToRotation(normVec, Vector3.forward) * planePoints[i];

            if (i == 0) PlaneZ = (Quaternion.FromToRotation(normVec, Vector3.forward) * planePoints[0]).z;

            minX = (minX > planePointsXY[i].x || i == 0) ? planePointsXY[i].x : minX;
            maxX = (maxX < planePointsXY[i].x || i == 0) ? planePointsXY[i].x : maxX;
            minY = (minY > planePointsXY[i].y || i == 0) ? planePointsXY[i].y : minY;
            maxY = (maxY < planePointsXY[i].y || i == 0) ? planePointsXY[i].y : maxY;
        }
    }
    public Vector3 WorldToPlaneXY(Vector3 pt) 
    {
        // Distance of vector to plane
        float dist = Vector3.Dot(normVec.normalized, (pt - closePoint));

        Vector3 toCam = Camera.main.transform.position - pt;

        // Finding vector parallel that of camera to point with same size component in plane norm direction
        Vector3 toPlaneTowardsCam = dist / Mathf.Cos(Mathf.Deg2Rad * Vector3.Angle(toCam, normVec)) * toCam.normalized;


        return pt - toPlaneTowardsCam;
    }

    public void CloseFarPointUpdate()
    {
        closePoint = perimPoints[0];
        farPoint = perimPoints[0];
        float minDistance = Vector3.Distance(perimPoints[0], Camera.main.transform.position);
        float maxDistance = Vector3.Distance(perimPoints[0], Camera.main.transform.position);

        for (int i = 1; i < perimPoints.Count; i++)
        {
            float tempDistance = Vector3.Distance(perimPoints[i], Camera.main.transform.position);
            if (minDistance > tempDistance)
            {
                minDistance = tempDistance;
                closePoint = perimPoints[i];
            }
            if (maxDistance < tempDistance)
            {
                maxDistance = tempDistance;
                farPoint = perimPoints[i];
            }
        }
        normVec = Camera.main.transform.position - closePoint;
    }
    public void LoopSelect(bool selectBool)
    {
        var parentClass = transform.parent.GetComponent<PointPlacer>();
        GenPlane();
        GameObject[] selectableObjs = GameObject.FindGameObjectsWithTag("Barrier");

        for (int i = 0; i < selectableObjs.Length; i++)
        {
            if(selectableObjs[i].name.Contains("Object"))
            {
                Vector3 currentObjPos = selectableObjs[i].transform.position;
                Vector3 currentObjPosXY = Quaternion.FromToRotation(normVec, Vector3.forward) * WorldToPlaneXY(currentObjPos);

                if (ContainsPoint(planePointsXY.ToArray(), currentObjPosXY))
                {
                    if (selectBool)
                    {
                        parentClass.selectedObjs.Add(selectableObjs[i]);
                        selectableObjs[i].GetComponent<MeshRenderer>().material = parentClass.selectedMaterial;
                    }
                    else
                    {
                        parentClass.selectedObjs.Remove(selectableObjs[i]);
                        selectableObjs[i].GetComponent<MeshRenderer>().material = parentClass.baseMaterial;
                    }

                }
            }
        }
        Destroy(transform.gameObject);
    }

    public IEnumerator GenSpaceFillingCurve()
    {
        GenPlane();
        spaceFillingCurveXY.Clear();
        spaceFillingCurvePlane.Clear();
        spaceFillingCurve.Clear();
        liftPointIndex.Clear();

        List<int> pointIndexToCheck = new List<int>();

        var parentClass = transform.GetComponentInParent<PointPlacer>();

        Vector3 prevNormVec = meanPoint - Camera.main.transform.position;
        Vector3 prevPlacedPoint = Vector3.zero;
        float lineThickness = parentClass.lineThickness;
        bool shiftX = false;
        bool segInY = false;
        bool prevOutBounds = false;
        bool closeCheck = false;
        bool reachable = true;

        Vector3 currentVec = new Vector3(minX, minY, PlaneZ);
        bool firstHit = false;
        float adjFillingCurvex = fillingCurvex / Vector3.Distance(farPoint,Camera.main.transform.position);
        float adjFillingCurvey = fillingCurvey / Vector3.Distance(farPoint, Camera.main.transform.position);
        if (adjFillingCurvex < fillingCurveLimitx) adjFillingCurvex = fillingCurveLimitx;

        Vector3 camPos = Camera.main.transform.position;

        float deltaAngle = 0;

        float deltaPlane = 0;

        while (currentVec.x < maxX || currentVec.x == adjFillingCurvex)
        {
            if (ContainsPoint(planePointsXY.ToArray(), currentVec))
            {
                Quaternion planeToXY = Quaternion.FromToRotation(normVec, Vector3.forward);

                Vector3 inPlane = Quaternion.Inverse(planeToXY) * currentVec;

                RaycastHit hit;

                Vector3 ray = inPlane - camPos;

                if (Physics.Raycast(camPos, ray, out hit, 100, drawMask))
                {
                    reachable = transform.GetComponentInParent<PointPlacer>().Reachable(transform.TransformPoint(hit.point));
                    if (reachable || !prevOutBounds)
                    {
                        spaceFillingCurveXY.Add(currentVec);
                        spaceFillingCurvePlane.Add(inPlane);
                        spaceFillingCurve.Add(hit.point);
                        closeCheck = !transform.GetComponentInParent<PointPlacer>().PreliminaryGripperCheck(transform.TransformPoint(hit.point));

                        if (!firstHit)
                        {
                            parentClass.NewObj(hit.point, hit.normal);
                            prevPlacedPoint = hit.point;
                            firstHit = true;
                            shiftX = false;
                            if (!reachable) parentClass.prevPoint.GetComponent<MeshRenderer>().material.color = Color.red;
                            Destroy(parentClass.prevSeg.GetComponent<LineCollider>());

                        }
                        else
                        {
                            if (parentClass.pointMade)
                            {
                                parentClass.StartLine();
                                parentClass.UpdateLine(hit.point, hit.normal);
                                prevPlacedPoint = hit.point;
                                if (!reachable) parentClass.prevPoint.GetComponent<MeshRenderer>().material.color = Color.red;
                                Destroy(parentClass.prevSeg.GetComponent<LineCollider>());
                                segInY = true;
                            }
                            else
                            {
                                deltaAngle = Vector3.Angle(prevNormVec, hit.normal);
                                
                                deltaPlane = (parentClass.prevPoint != null) ? Vector3.Dot((hit.point - parentClass.prevPoint.transform.position), prevNormVec.normalized) : 0f;

                                if (!reachable || ((Vector3.Distance(hit.point, prevPlacedPoint) > pointDeltaThreshod | deltaPlane > adjFillingCurvex | deltaAngle > planeAngleThreshold)) | shiftX | !segInY | closeCheck)
                                {

                                    parentClass.UpdateLine(hit.point, hit.normal);
                                    if (!reachable) parentClass.prevPoint.GetComponent<MeshRenderer>().material.color = Color.red;
                                    Destroy(parentClass.prevSeg.GetComponent<LineCollider>());
                                    prevPlacedPoint = hit.point;
                                    if (shiftX)
                                    {
                                        parentClass.prevLine.GetComponent<LineHolder>().liftPointIndex.Add(transform.GetComponentInParent<PointPlacer>().pointSegs.Count - 1);
                                        parentClass.prevSeg.SetActive(false);
                                        shiftX = false;

                                    }
                                    else if (!segInY) segInY = true;
                                }
                                else
                                {
                                    parentClass.newPoints[parentClass.newPoints.Count - 1] = hit.point;
                                    parentClass.prevPoint.transform.position = hit.point;
                                    parentClass.prevSeg.transform.localScale = new Vector3(lineThickness, Vector3.Distance(parentClass.newPoints[parentClass.newPoints.Count - 2], hit.point) / 2, lineThickness);
                                    parentClass.prevSeg.transform.position = Vector3.Lerp(parentClass.newPoints[parentClass.newPoints.Count - 2], hit.point, 0.5f);

                                }
                            }
                        }
                        prevNormVec = hit.normal;
                        if (closeCheck && !(deltaAngle > planeAngleThreshold) && !(deltaPlane > adjFillingCurvex))
                        {
                            pointIndexToCheck.Add(parentClass.newPoints.Count-1);
                            Debug.Log(parentClass.newPoints.Count - 1);
                        }
                    }
                    else 
                    {
                        shiftX = true;
                        segInY = false;
                    }
                    if (!reachable) prevOutBounds = true;
                    else prevOutBounds = false;
                }
            }
            else 
            {
                shiftX = true;
                segInY = false;
            }
            if (currentVec.y > maxY)
            {
                currentVec.y = minY - adjFillingCurvey;
                currentVec.x += adjFillingCurvex;
                shiftX = true;
                segInY = false;
                prevOutBounds = false;
                yield return new WaitForSeconds(0.1f);
                if (pointIndexToCheck.Count > 2) SimplifyLines(pointIndexToCheck, parentClass);
                pointIndexToCheck.Clear();
            }
            if (!closeCheck && (reachable || prevOutBounds)) currentVec.y += adjFillingCurvey;

            else currentVec.y += adjFillingCurvey / 4.0f;

            if (parentClass.newPoints.Count %2 == 0) yield return null;
        }
        parentClass.FinishLine();
        parentClass.AddLineToAllPoints();
        parentClass.loading = false;
        Destroy(transform.gameObject);
    }

    public void SimplifyLines(List<int> pointIndexToCheck, PointPlacer pointPlacer)
    {
        bool previousActive = false;
        bool middleSegment = false;
        float lineThickness = pointPlacer.lineThickness;
        Vector3 startSegment = new Vector3();
        GameObject segToExtend = new GameObject();

        for (int i = 0; i<pointIndexToCheck.Count; i++)
        {
            bool currentActive = (pointPlacer.pointSegs[pointIndexToCheck[i]] != null) && (pointPlacer.pointSegs[pointIndexToCheck[i]].GetComponent<MeshRenderer>().material.color != Color.red);

            if ((middleSegment) && currentActive) // previous two and current are active, delete previous
            {
                Destroy(pointPlacer.pointSegs[pointIndexToCheck[i - 1]]);
                if (i < pointIndexToCheck.Count - 1 && pointPlacer.lineSegs[pointIndexToCheck[i]] != null) Destroy(pointPlacer.lineSegs[pointIndexToCheck[i]]);
                segToExtend.transform.localScale = new Vector3(lineThickness, Vector3.Distance(startSegment, pointPlacer.newPoints[pointIndexToCheck[i]]) / 2, lineThickness);
                segToExtend.transform.position = Vector3.Lerp(startSegment, pointPlacer.newPoints[pointIndexToCheck[i]], 0.5f);
            }

            if ((currentActive && previousActive))
            {
                if (!middleSegment)
                {
                    startSegment = pointPlacer.newPoints[pointIndexToCheck[i - 1]];
                    if (i < pointIndexToCheck.Count - 1) segToExtend = pointPlacer.lineSegs[pointIndexToCheck[i]];
                }
                middleSegment = true;
            }
            else middleSegment = false;

            if (!currentActive)
            {
                Destroy(pointPlacer.pointSegs[pointIndexToCheck[i]]);
                if (i < pointIndexToCheck.Count - 1 && pointPlacer.lineSegs[pointIndexToCheck[i]] != null) Destroy(pointPlacer.lineSegs[pointIndexToCheck[i]]);
                if (previousActive && pointPlacer.lineSegs[pointIndexToCheck[i - 1]] != null) Destroy(pointPlacer.lineSegs[pointIndexToCheck[i - 1]]);
            }

            previousActive = currentActive && (i == pointIndexToCheck.Count - 1 || (Mathf.Abs(pointIndexToCheck[i+1] - pointIndexToCheck[i]) == 1));
        }
        pointPlacer.RemoveCheckers();
    }

    // Author: Eric Haines (Eric5h5)
    // https://wiki.unity3d.com/index.php/PolyContainsPoint

    public bool ContainsPoint(Vector2[] polyPoints, Vector2 p)
    {
        var j = polyPoints.Length - 1;
        var inside = false;
        for (int i = 0; i < polyPoints.Length; j = i++)
        {
            var pi = polyPoints[i];
            var pj = polyPoints[j];
            if (((pi.y <= p.y && p.y < pj.y) || (pj.y <= p.y && p.y < pi.y)) &&
                (p.x < (pj.x - pi.x) * (p.y - pi.y) / (pj.y - pi.y) + pi.x))
                inside = !inside;
        }
        return inside;
    }
}