﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Input;
using RosSharp;
using TMPro;
using Microsoft.MixedReality.Toolkit;


public class PointPlacer : MonoBehaviour
{
    // Customisable variables
    public GameObject pointFab;
    public GameObject lineSegFab;
    public GameObject lineHolderFab;
    public float lineThickness;
    public float spawnRad;
    public float loopRadThreshold;
    public LayerMask drawMask;
    public GameObject voiceCommander;
    public Material selectedMaterial;
    public Material baseMaterial;
    public float selectionClearance;
    public float workSpaceRad;
    public float robotRad;
    public GameObject robotCollider;
    public GameObject ROSConnector;
    public GameObject miniMenu;
    public GameObject robotMenu;
    public GameObject workspaceMenu;
    public GameObject slimMenu;
    public GameObject ClearPointsMenu;
    public Material buttonMaterial;
    public Material activeMaterial;
    public Material hoverMaterial;
    public GameObject gripperBounds;
    public GameObject pickBounds;

    [SerializeField]
    public TextMeshPro commandText;

    public RosSharp.RosBridgeClient.SceneWatcher SceneWatcher;

    // Class variables
    public GameObject prevSeg; // previous line segment
    public List<Vector3> newPoints; // all points in the current object being drawn
    public List<GameObject> lineSegs; // list of all line segments
    public List<GameObject> pointSegs; // list of all point objects
    public List<GameObject> checkerObjsGrip; // list of all point objects
    public List<GameObject> checkerObjsPick; // list of all point objects
    public GameObject prevLine; // previous line holding object
    public List<Vector3> allPoints; // list of all the active points
    public List<GameObject> selectedObjs; // list of object selected
    public GameObject prevPoint; // previous point object
    public int segmentBuffer; // number of segments needed before a loop can be formed
    public bool pointMade;  // Flag for first point made
    public bool pointerUp; // Flag for the active pointer being in the up position
    public bool autoMade; // Determines if drawing points are user made or generated from another function
    public bool placeFlag;
    public GameObject activeMenu;
    public bool miniMenuHit;
    public bool gripperCheck = true;
    public bool loading = false;
    public List<Vector3> toClear;

    private int nameNum; // Counter for segment naming convention
    private bool endLoop; // Flag for if a loop is completed
    private bool blockDraw = false;
    private Vector3 miniMenuHitLast = Vector3.zero;
    private GameObject activeButton;
    private float gripperRad;
    public bool waitFlag;

    private void Start()
    {
        // Initialising variables
        nameNum = 0;
        pointMade = false;
        endLoop = true;
        autoMade = true;
        pointerUp = true;
        gripperRad = gripperBounds.transform.localScale.magnitude / 2.0f;

        if (SceneWatcher.pickedObj != null)
        {
            pickBounds = Instantiate(SceneWatcher.pickedObj);
            pickBounds.transform.localScale *= 1.0f;
            pickBounds.tag = "Checker";
            Destroy(pickBounds.GetComponent<BarrierCollider>());
            Destroy(pickBounds.GetComponent<MeshRenderer>());
            pickBounds.SetActive(true);
        }

    }
    // Update is called once per frame
    void Update()
    {

    }

    public void CleanLine()
    {
        for (int i = 0; i < pointSegs.Count; i++)
        {
            if (pointSegs[i] != null)
            {
                if (pointSegs[i].GetComponent<MeshRenderer>().material.color == Color.red)
                {
                    if (i != pointSegs.Count - 1) Destroy(lineSegs[i]);
                    if (i != 0) Destroy(lineSegs[i - 1]);
                }
                Destroy(pointSegs[i]);
            }
        }
        foreach (var lineSeg in lineSegs)
        {
            if (lineSeg != null)
            {
                Destroy(lineSeg.GetComponent<BoxCollider>());
                Destroy(lineSeg.GetComponent<Rigidbody>());
                if (lineSeg.GetComponent<MeshRenderer>().material.color == Color.red)
                {
                    Destroy(lineSeg);
                }
            }
        }
    }

    // Locks points into line or loop
    public void MakeLoop()
    {
        if (!autoMade)
        {
            CloseMenu();

            if (commandText!=null) commandText.text = "To Line";
            autoMade = true;
            AddLineToAllPoints();
        }
    }

    // Calls loop select func of previous loop
    public void Select()
    {
        if (!autoMade)
        {
            RemoveCheckers();
            autoMade = true;
            prevLine.GetComponent<LineHolder>().LoopSelect(true);
        }
    }

    public void AutoPick()
    {
        if (!autoMade)
        {
            UnselectAll();
            RemoveCheckers();
            CloseMenu();
            if (SceneWatcher.pickedObj != null) SceneWatcher.pickedObj.tag = "Barrier";

            Select();
            voiceCommander.GetComponent<VoiceCommands>().Pick();
        }
    }

    // Calls loop select func of previous loop in unselect mode
    public void Unselect()
    {
        if (!autoMade)
        {
            RemoveCheckers();
            CloseMenu();
            if (commandText != null) commandText.text = "Unselect";
            autoMade = true;
            prevLine.GetComponent<LineHolder>().LoopSelect(false);
        }
    }

    // Removes all objects from selection
    public void UnselectAll()
    {
        if (commandText!=null) commandText.text = "Unselect All";
        if (selectedObjs != null || selectedObjs.Count < 1)
        {

            foreach (var obj in selectedObjs)
            {
                obj.GetComponent<MeshRenderer>().material = baseMaterial;
            }
            selectedObjs.Clear();
        }
    }

    public void ClearLoop(bool pointBarrier01)
    {
        if (!loading)
        {
            StartCoroutine(ClearLoopRoutine(pointBarrier01));
        }
    }

    public IEnumerator ClearLoopRoutine(bool pointBarrier01)
    {
        Debug.Log(autoMade.ToString() + " 202");
        if (!autoMade)
        {
            RemoveCheckers();
            CloseMenu();
            autoMade = true;

            toClear.Clear();
            GameObject[] barriers = GameObject.FindGameObjectsWithTag("Barrier");

            if (pointBarrier01)
            {
                if (commandText != null) commandText.text = "Clear Barriers";

                for (int i = 0; i < barriers.Length; i++)
                {
                    toClear.Add(barriers[i].transform.position);
                }
            }
            else
            {

                foreach (var point in GameObject.FindGameObjectsWithTag("clone"))
                {
                    toClear.Add(point.transform.position);
                }
                allPoints.Clear();
                voiceCommander.GetComponent<VoiceCommands>().ClearPoints();
            }
            loading = true;
            LineHolder loopClass = prevLine.GetComponent<LineHolder>();

            for (int i = 0; i < toClear.Count; i++)
            {
                loopClass.GenPlane();
                Vector3 currentObjPosXY = Quaternion.FromToRotation(loopClass.normVec, Vector3.forward) * loopClass.WorldToPlaneXY(toClear[i]);
                Debug.Log(loopClass.ContainsPoint(loopClass.planePointsXY.ToArray(), currentObjPosXY));
                if (loopClass.ContainsPoint(loopClass.planePointsXY.ToArray(), currentObjPosXY))
                {
                    if (pointBarrier01)
                    {
                        voiceCommander.GetComponent<VoiceCommands>().RemoveThisBarrier(barriers[i]);
                    }
                }
                else if (!pointBarrier01)
                {
                    allPoints.Add(toClear[i]);
                    Debug.Log(i);
                }
            }
            Destroy(prevLine);
            if (!pointBarrier01)
            {
                if (commandText != null) commandText.text = "Clear Points";
                for (int i = 0; i < allPoints.Count; i++)
                {
                    if (i == 0) NewObj(allPoints[i], Vector3.up);
                    else
                    {
                        if (i == 1) StartLine();
                        UpdateLine(allPoints[i], Vector3.up);
                        Destroy(prevSeg.GetComponent<BoxCollider>());
                        Destroy(prevSeg.GetComponent<Rigidbody>());
                    }

                    voiceCommander.GetComponent<VoiceCommands>().AddToPath(allPoints[i]);
                    yield return null;
                }
            }
        }
        loading = false;
        Debug.Log("finish routine");
        RemoveCheckers();
    }

    // Places point above each selected object
    public void SelectionToPoint()
    {
        if (selectedObjs.Count > 0)
        {
            foreach (var obj in selectedObjs)
            {
                NewObj(obj.transform.position + new Vector3(0, selectionClearance + obj.transform.localScale.y / 2, 0), obj.transform.up);
                PointerUp();
            }
        }
    }

    // Calls space filling curve generation of previous loop
    public void MakeFill()
    {
        CloseMenu();
        if (!autoMade)
        {
            RemoveCheckers();
            if (commandText!=null) commandText.text = "Fill";
            autoMade = true;
            loading = true;
            bool anyValid = false;
            foreach (var point in pointSegs)
            {
                if (point.GetComponent<MeshRenderer>().material.color != Color.red)
                {
                    anyValid = true;
                    break;
                }
            }
            if (anyValid) StartCoroutine(prevLine.GetComponent<LineHolder>().GenSpaceFillingCurve());
            else
            {
                Destroy(prevLine);
                loading = false;
            }
        }
    }

    // Places a point at the mean position of the previous loop
    public void ToPoint()
    {
        if (!autoMade)
        {
            if (commandText!=null) commandText.text = "Point";
            autoMade = true;
            prevLine.GetComponent<LineHolder>().CloseFarPointUpdate();
            Vector3 meanPoint = prevLine.GetComponent<LineHolder>().meanPoint;
            Vector3 camPos = Camera.main.transform.position;
            RaycastHit hit;

            // Ray from camera to mean point
            if (Physics.Raycast(camPos, meanPoint - camPos, out hit, 100, drawMask))
            {
                NewObj(hit.point, hit.normal);
                PointerUp();
            }
            Destroy(prevLine);
        }
    }

    public void Place()
    {
        if (allPoints.Count != 0 && !voiceCommander.GetComponent<VoiceCommands>().inMotion)
        {
            StartCoroutine(SceneWatcher.PlaceRoutine(allPoints[allPoints.Count-1]));
            placeFlag = false;
        }
    }
    // Sends all generated or drawn points to be sent to the ROS connector
    public void AddLineToAllPoints()
    {
        StartCoroutine(AddRoutine());
    }

    public void QuickAdd()
    {
        for (int i = 0; i < newPoints.Count; i++)
        {
            if (pointSegs[i] != null && pointSegs[i].GetComponent<MeshRenderer>().material.color != Color.red)
            {
                allPoints.Add(newPoints[i]);
                voiceCommander.GetComponent<VoiceCommands>().AddToPath(newPoints[i]);
            }
        }
        CleanLine();
        RemoveCheckers();
        loading = false;
    }

    public IEnumerator AddRoutine()
    {
        loading = true;
        yield return new WaitForSeconds(0.1f);
        QuickAdd();
    }

    public void RemoveCheckers()
    {
        for (int i = 0; i < checkerObjsGrip.Count; i++)
        {
            if (checkerObjsGrip[i])
            {
                if (placeFlag) Destroy(checkerObjsPick[i]);
                Destroy(checkerObjsGrip[i]);
            }
        }
        checkerObjsPick.Clear();
        checkerObjsGrip.Clear();
    }

    public IEnumerator Wait(float time)
    {
        yield return new WaitForSeconds(time);
    }

    public void PointerDown(MixedRealityPointerEventData eventData)
    {
        if (pointerUp && !blockDraw) FirstPoint(eventData);
    }

    public void PointerDrag(MixedRealityPointerEventData eventData)
    {
        if (!blockDraw)
        {
            Debug.Log(pointMade);
            if (pointerUp) FirstPoint(eventData);
            else NextPoint(eventData);
        }
    }

    public void PointerUp()
    {
        pointerUp = true;
        if (!blockDraw)
        {
            blockDraw = true;
            StartCoroutine(WaitForUp(0.2f));
            // If point made add to all points and reset for next input
            Debug.Log(pointMade);
            if (pointMade && !loading)
            {
                AddLineToAllPoints();
                autoMade = true;
                pointMade = false;
            }
            // If line made but not complete loop finish off line as if it were a loop
            else if (activeMenu == null)
            {
                if (!autoMade && !loading)
                {
                    if (!endLoop) FinishLine();
                    //miniMenu.SetActive(true);
                    //miniMenu.transform.position = prevLine.GetComponent<LineHolder>().meanPoint + Vector3.up * 0.3f;
                    //activeMenu = miniMenu;
                    //Debug.Log("Make Menu" + blockDraw.ToString() + autoMade.ToString());
                }
            }
            else
            {
                if (miniMenuHit) MenuSelection(true);
                CloseMenu();
            }
        }
    }

    public void CloseMenu()
    {
        miniMenu.SetActive(false);
        blockDraw = false;
        activeMenu = null;
        miniMenuHit = false;
        Debug.Log("Close Menu");
    }

    public IEnumerator WaitForUp(float time) {
        yield return new WaitForSeconds(time);
        if (pointerUp) blockDraw = false;
    }

    public void MenuSelection(bool selecting)
    {
        if (activeButton != null) activeButton.GetComponent<MeshRenderer>().material = buttonMaterial;

        int quadrant = 0;
        if (miniMenuHitLast.y < miniMenuHitLast.x) quadrant += 2;
        if (miniMenuHitLast.y < -miniMenuHitLast.x) quadrant += 1;

        if (activeMenu.name == miniMenu.name)
        {
            switch (quadrant)
            {
                case 0:
                    activeButton = GameObject.Find(activeMenu.name + "1");
                    if (selecting) AutoPick();
                    break;
                case 1:
                    activeButton = GameObject.Find(activeMenu.name + "2");
                    if (selecting) ClearLoop(true);
                    break;
                case 2:
                    activeButton = GameObject.Find(activeMenu.name + "3");
                    if (selecting) ClearLoop(false);
                    break;
                case 3:
                    activeButton = GameObject.Find(activeMenu.name + "4");
                    if (selecting) MakeFill();
                    break;

            }
        }
        else if (activeMenu.name == robotMenu.name)
        {
            switch (quadrant)
            {
                case 0:
                    activeButton = GameObject.Find(activeMenu.name + "1");
                    if (selecting) voiceCommander.GetComponent<VoiceCommands>().RobotRun();
                    break;
                case 1:
                    activeButton = GameObject.Find(activeMenu.name + "2");
                    if (selecting) voiceCommander.GetComponent<VoiceCommands>().ToUser();
                    break;
                case 2:
                    activeButton = GameObject.Find(activeMenu.name + "3");
                    if (selecting) voiceCommander.GetComponent<VoiceCommands>().ReadyState();
                    break;
                case 3:
                    activeButton = GameObject.Find(activeMenu.name + "4");
                    if (selecting) voiceCommander.GetComponent<VoiceCommands>().Place();
                    break;

            }
        }
        else if (activeMenu.name == workspaceMenu.name)
        {
            switch (quadrant)
            {
                case 0:
                    activeButton = GameObject.Find(activeMenu.name + "1");
                    if (selecting) GameObject.Find("Workspace").GetComponent<WorkplaceScanner>().ManualRefresh();
                    break;
                case 1:
                    activeButton = GameObject.Find(activeMenu.name + "2");
                    if (selecting) voiceCommander.GetComponent<VoiceCommands>().ClearBarriers();
                    break;
                case 2:
                    activeButton = GameObject.Find(activeMenu.name + "3");
                    if (selecting) voiceCommander.GetComponent<VoiceCommands>().ClearPoints();
                    break;
                case 3:
                    activeButton = GameObject.Find(activeMenu.name + "4");
                    if (selecting) UnselectAll();
                    break;
            }
        }
        else if (activeMenu.name == slimMenu.name)
        {
            switch (quadrant)
            {
                case 0:
                    activeButton = GameObject.Find(activeMenu.name + "1");
                    if (selecting) voiceCommander.GetComponent<VoiceCommands>().RobotRun();
                    break;
                case 1:
                    activeButton = GameObject.Find(activeMenu.name + "2");
                    if (selecting)
                    {
                        AutoPick();
                        voiceCommander.GetComponent<VoiceCommands>().RobotRun();
                    }
                    break;
                case 2:
                    activeButton = GameObject.Find(activeMenu.name + "3");
                    if (selecting)
                    {
                        voiceCommander.GetComponent<VoiceCommands>().Place();
                        voiceCommander.GetComponent<VoiceCommands>().RobotRun();
                        UnselectAll();
                    }
                    break;
                case 3:
                    activeButton = GameObject.Find(activeMenu.name + "4");
                    if (selecting)
                    {
                        StartCoroutine(FillAndRun());
                    }
                    break;
            }
        }
        else if (activeMenu.name == ClearPointsMenu.name)
        {
            activeButton = GameObject.Find(activeMenu.name + "1");
            if (selecting) voiceCommander.GetComponent<VoiceCommands>().ClearPoints();
        }
        if (activeButton != null)
        {
            if (selecting)
            {
                activeButton.GetComponent<MeshRenderer>().material = activeMaterial;
                StartCoroutine(RevertColour(activeButton));
            }
            else activeButton.GetComponent<MeshRenderer>().material = hoverMaterial;
        }
    }

    public IEnumerator FillAndRun()
    {
        MakeFill();
        yield return new WaitForSeconds(1f);
        while (loading)
        {
            yield return new WaitForSeconds(1f);
        }
        voiceCommander.GetComponent<VoiceCommands>().RobotRun();
    }

    public IEnumerator RevertColour(GameObject toRevert)
    {
        yield return new WaitForSeconds(1f);
        toRevert.GetComponent<MeshRenderer>().material = buttonMaterial;
    }

    // Creates first drawn point
    public void FirstPoint(MixedRealityPointerEventData eventData)
    {

        IPointerResult result = eventData.Pointer.Result;

        // Ignore gaze pointer
        if (eventData.Pointer.GetType().ToString() != "Microsoft.MixedReality.Toolkit.Input.GGVPointer")
        {
            Vector3 camPos = Camera.main.transform.position;
            RaycastHit hit;

            // Ray from camera to mean point
            if (Physics.Raycast(camPos, result.Details.Point - camPos, out hit, 100, drawMask))
            {
                if ((activeMenu != null) && (activeMenu.name == miniMenu.name) && (hit.transform.parent != null) && (hit.transform.parent.name == miniMenu.name))
                {
                    miniMenuHit = true;
                    miniMenuHitLast = miniMenu.transform.InverseTransformPoint(hit.point);
                    pointerUp = false;
                    Debug.Log("Loop Menu");
                    MenuSelection(false);
                }
                else if (hit.transform.tag == "MiniMenu")
                {
                    activeMenu = hit.transform.parent.gameObject;
                    miniMenuHitLast = activeMenu.transform.InverseTransformPoint(hit.point);
                    pointerUp = false;
                    miniMenuHit = true;
                    miniMenu.SetActive(false);
                    Debug.Log("Side Menu");
                    MenuSelection(false);
                }
                else if (!loading)
                {
                    // If the previous object was user drawn, send its points to the ROS connector before creating more drawn points
                    if (!autoMade) QuickAdd();
                    NewObj(hit.point, hit.normal);
                    voiceCommander.GetComponent<VoiceCommands>().toMePoint = Vector3.zero;
                    pointerUp = false;
                    Debug.Log("PointMade");
                    CloseMenu();
                }
            }
        }
    }

    // Adds second and subsequent points and line segments
    public void NextPoint(MixedRealityPointerEventData eventData)
    {
        IPointerResult result = eventData.Pointer.Result;
        // Do not use Gaze pointer
        if (eventData.Pointer.GetType().ToString() != "Microsoft.MixedReality.Toolkit.Input.GGVPointer")
        {
            Vector3 camPos = Camera.main.transform.position;
            RaycastHit hit;

            // Ray from camera to mean point
            if (Physics.Raycast(camPos, result.Details.Point - camPos, out hit, 100, drawMask))
            {
                if (miniMenuHit)
                {
                    miniMenuHitLast = activeMenu.transform.InverseTransformPoint(hit.point);
                    Debug.Log(hit.point);
                    MenuSelection(false);
                }
                else if (!endLoop)
                {
                    // Find the distance between previous and current points
                    float deltaPos = Vector3.Distance(newPoints[newPoints.Count - 1], hit.point);

                    // If distance between previous and current points over threshold for second point or for subsequent points
                    if ((deltaPos >= loopRadThreshold | (deltaPos >= spawnRad && !pointMade)))
                    {

                        // Form line object if second point
                        if (pointMade)
                        {
                            autoMade = false;
                            StartLine();
                        }
                        // Updated the line with next point
                        UpdateLine(hit.point, hit.normal);
                    }
                }
            }
        }
    }

    public bool InBounds(int index)
    {
        if (Reachable(newPoints[index]))
        {
            if (!PreliminaryGripperCheck(newPoints[index]))
            {
                StartCoroutine(GripperCheck(index));
                return true;
            }
            else return true;
        }
        return false;
    }

    public bool Reachable(Vector3 pt)
    {
        if (Vector3.Distance(Vector3.ProjectOnPlane(robotCollider.transform.position, new Vector3(0, 1, 0)), Vector3.ProjectOnPlane(pt, new Vector3(0, 1, 0))) >= robotRad)
        {
            if (Vector3.Distance(Vector3.ProjectOnPlane(robotCollider.transform.position, new Vector3(0, 1, 0)), Vector3.ProjectOnPlane(pt, new Vector3(0, 1, 0))) <= workSpaceRad)
            {
                return true;
            }
        }
        return false;
    }

        public bool PreliminaryGripperCheck(Vector3 pt)
    {
        foreach (var barrier in GameObject.FindGameObjectsWithTag("Barrier"))
        {
            float barrierRad = barrier.transform.localScale.magnitude / 2.0f;
            if (Vector3.Distance(transform.TransformPoint(barrier.transform.position), transform.TransformPoint(pt + gripperBounds.transform.localScale.y * Vector3.up / 2.0f)) < gripperRad + barrierRad) return false;
            if (placeFlag)
            {
                float pickedObjRad = SceneWatcher.pickedObj.transform.localScale.magnitude / 2.0f;
                if (Vector3.Distance(transform.TransformPoint(barrier.transform.position), transform.TransformPoint(pt + SceneWatcher.pickedObj.transform.localScale.y * Vector3.up / 2.0f)) < pickedObjRad + barrierRad) return false;
            }
        }
        return true;
    }

    public IEnumerator GripperCheck(int index)
    {
        while (pointSegs[index] == null) yield return null;
        GameObject gripperTemp = Instantiate(gripperBounds, pointSegs[index].transform.position + Vector3.up * gripperBounds.transform.localScale.y / 2.0f, transform.rotation);
        gripperTemp.tag = "Checker";
        if (index != 0) gripperTemp.GetComponent<LinkLine>().lineSeg = lineSegs[index - 1];
        gripperTemp.GetComponent<LinkLine>().pointSeg = pointSegs[index];
        checkerObjsGrip.Add(gripperTemp);

        if (placeFlag)
        {
            GameObject pickObjTemp = Instantiate(pickBounds, pointSegs[index].transform.position + Vector3.up * pickBounds.transform.localScale.y / 2.0f, transform.rotation);
            pickObjTemp.transform.localScale = 1.1f * pickObjTemp.transform.localScale;
            pickObjTemp.tag = "Checker";

            pickObjTemp.AddComponent<LinkLine>();

            if (index != 0) pickObjTemp.GetComponent<LinkLine>().lineSeg = lineSegs[index - 1];
            pickObjTemp.GetComponent<LinkLine>().pointSeg = pointSegs[index];
            pointSegs[index].name = "Attach " + index.ToString();
            Debug.Log(pointSegs[index].name);


            pickObjTemp.GetComponent<LinkLine>().otherObj = gripperTemp;
            gripperTemp.GetComponent<LinkLine>().otherObj = pickObjTemp;

            checkerObjsPick.Add(pickObjTemp);
        }
    }

    // Creates a new point object which can then be converted to a lineHolding object later
    public void NewObj(Vector3 point, Vector3 norm)
    {
        endLoop = false;
        newPoints.Clear();
        pointSegs.Clear();
        MakePoint(point, norm);
        prevPoint.transform.parent = gameObject.transform;
        pointMade = true;
        if (!InBounds(newPoints.Count - 1))
        {
            Debug.Log("OutBound");
            pointSegs[newPoints.Count - 1].GetComponent<MeshRenderer>().material.color = Color.red;
        }
    }

    // Spawns a point object at location with orientation
    public void MakePoint(Vector3 point, Vector3 norm)
    {
        newPoints.Add(point);
        prevPoint = Instantiate(pointFab, point, Quaternion.LookRotation(norm));
        prevPoint.transform.localScale = new Vector3(lineThickness, lineThickness, lineThickness);
        pointSegs.Add(prevPoint);
    }

    // Replaces point object with a line holding object
    public void StartLine()
    {
        pointMade = false;
        prevLine = Instantiate(lineHolderFab, Vector3.zero, Quaternion.identity);
        prevLine.transform.parent = gameObject.transform;
        prevPoint.transform.parent = prevLine.transform;

        lineSegs.Clear();
    }

    //
    public void UpdateLine(Vector3 pt, Vector3 norm)
    {
        MakePoint(pt, norm);

        Vector3 pt0 = newPoints[newPoints.Count - 2];

        prevPoint.transform.parent = prevLine.transform;

        prevSeg = Instantiate(lineSegFab, Vector3.Lerp(pt0, pt, 0.5f), Quaternion.identity);
        prevSeg.transform.parent = prevLine.transform;

        prevSeg.transform.localScale = new Vector3(lineThickness, Vector3.Distance(pt0, pt) / 2, lineThickness);
        prevSeg.transform.rotation = Quaternion.FromToRotation(Vector3.up, (pt0 - pt).normalized);
        prevSeg.name = nameNum.ToString();
        nameNum++;

        lineSegs.Add(prevSeg);
        if (!InBounds(newPoints.Count - 1))
        {
            lineSegs[newPoints.Count - 2].GetComponent<MeshRenderer>().material.color = Color.red;
            pointSegs[newPoints.Count - 1].GetComponent<MeshRenderer>().material.color = Color.red;
        }
    }

    public void FinishLoop(Collider col, GameObject callingObj)
    {
        if (!endLoop && callingObj == lineSegs[lineSegs.Count - 1].gameObject)
        {
            for (int i = 0; i < lineSegs.Count - 2 - segmentBuffer; i++)
            {
                if (col.gameObject == lineSegs[i].gameObject)
                {
                    //Del last segment to form corner later
                    newPoints.RemoveAt(newPoints.Count - 1);
                    DelSeg(newPoints.Count - 1);

                    //Adding new point midway along intersecting segment and making line segment to it
                    UpdateLine(lineSegs[i].transform.position, pointSegs[i].transform.up);

                    for (int j = i; j >= 0; j--)
                    {
                        DelSeg(j);
                        newPoints.RemoveAt(j);
                    }

                    GameObject toDel = pointSegs[0].gameObject;

                    //Adding point so that start is the end and adding segment
                    UpdateLine(newPoints[0], pointSegs[1].transform.up);

                    pointSegs[0] = Instantiate(pointFab, pointSegs[pointSegs.Count - 1].transform.position, pointSegs[pointSegs.Count - 1].transform.rotation);
                    pointSegs[0].transform.parent = prevLine.transform;
                    pointSegs[0].transform.localScale = new Vector3(lineThickness, lineThickness, lineThickness);
                    if (!InBounds(0))
                    {
                        pointSegs[0].GetComponent<MeshRenderer>().material.color = Color.red;
                    }
                    Destroy(toDel, 0f);

                    FinishLine();
                    break;
                }
            }
        }
    }
    public void DelSeg(int j) {
        GameObject toDel = lineSegs[j].gameObject;
        lineSegs.RemoveAt(j);
        Destroy(toDel, 0f);

        toDel = pointSegs[j + 1].gameObject;
        pointSegs.RemoveAt(j + 1);
        Destroy(toDel, 0f);
    }
    public void FinishLine() {
        endLoop = true;

        prevLine.GetComponent<LineHolder>().SetPoints(newPoints);
    }
}