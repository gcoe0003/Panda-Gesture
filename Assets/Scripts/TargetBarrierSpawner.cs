﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetBarrierSpawner : MonoBehaviour
{
    public GameObject voiceCommander;
    public int cubeSphere01;
    public float distThreshold;
    public float rotThreshold;
    public string keyCode;

    private Transform child;
    private Transform barrierTransform;
    private Vector3 prevPos;
    private Quaternion prevRot;
    

    private void Start()
    {
        child = transform.GetChild(0).transform;
    }

    private void Update()
    {
        if (Input.GetKeyDown(keyCode)) SpawnBarrier();
        if (barrierTransform != null && (distThreshold < Vector3.Distance(prevPos, child.transform.position) || rotThreshold < Quaternion.Angle(prevRot, child.transform.rotation)))
        {
            barrierTransform.position = child.transform.position;
            barrierTransform.rotation = child.transform.rotation;
            prevPos = barrierTransform.position;
            prevRot = child.transform.rotation;
        }
    }

    public void SpawnBarrier()
    {
        if (barrierTransform != null) Destroy(barrierTransform.gameObject);
        var voiceClass = voiceCommander.GetComponent<VoiceCommands>();
        barrierTransform = voiceClass.AddBarrierAtPoint(cubeSphere01, child.position, child.rotation, child.localScale);
        barrierTransform.gameObject.GetComponent<MeshRenderer>().enabled = false;
        barrierTransform.name = "ObjectBarrier" + VoiceCommands.barrierCount;
        prevPos = barrierTransform.position;
        prevRot = child.transform.rotation;
    }
}
