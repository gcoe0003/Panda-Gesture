﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class WorkplaceScanner : MonoBehaviour
{
    public float scanDelta;
    public float scanHeight;
    public float heightThreshold;
    public LayerMask drawMask;
    public GameObject testMarker;
    public GameObject voiceCommander;
    public GameObject robotCollider;
    public Transform [,] workplaceBarriers;

    [SerializeField]
    public TextMeshPro commandText;

    private bool firstScan;
    private bool refreshing;
    private float rad;

    // Start is called before the first frame update
    void Start()
    {
        refreshing = false;
        firstScan = true;
        rad = transform.localScale.x*1.1f/2;
        workplaceBarriers = new Transform[Mathf.FloorToInt(2*rad/scanDelta), Mathf.FloorToInt(2*rad / scanDelta)];
        // StartCoroutine(GenWorkspaceBarrier());
    }

    public void ManualRefresh()
    {
        if (!refreshing)
        {
            GameObject.Find("Points").GetComponent<PointPlacer>().CloseMenu();
            refreshing = true;
            commandText.text = "Refresh Workspace";
            StartCoroutine(ResfreshWorkspace());
            StopCoroutine(ResfreshWorkspace());
        }
    }

    public IEnumerator ResfreshWorkspace()
    {

        var voiceClass = voiceCommander.GetComponent<VoiceCommands>();
        if (!firstScan) VoiceCommands.barrierCount -= workplaceBarriers.Length;

        for (int ix = 0; ix < workplaceBarriers.GetLength(0); ix++)
        {
            float scanX = transform.position.x - rad + scanDelta * ix;
            Vector3 startPosPrelim = new Vector3(scanX, transform.position.y + heightThreshold, transform.position.z - rad);
            Vector3 rayVecPrelim = new Vector3(0, 0, 2 * rad);
            RaycastHit hitPrelim;

            bool checkX = (Physics.Raycast(startPosPrelim, rayVecPrelim, out hitPrelim, rad * 2, drawMask));

            Transform prevBarrier = null;

            for (int iz = 0; iz < workplaceBarriers.GetLength(1); iz++)
            {
                float scanZ = transform.position.z - rad + scanDelta * iz;
                Transform toSpawn = null;
                if (checkX)
                {
                    Vector3 startPos = new Vector3(scanX, transform.position.y+scanHeight, scanZ);
                    Vector3 rayVec = new Vector3(0, -scanHeight, 0);
                    RaycastHit hit;


                    if (Physics.Raycast(startPos, rayVec, out hit, 10, drawMask))
                    {

                        if (hit.point.y - transform.position.y > heightThreshold)
                        {
                            Vector3 scale = new Vector3(scanDelta * 1.8f, Mathf.Abs(hit.point.y - transform.position.y) + scanDelta/4, scanDelta * 1.8f);
                            Vector3 pos = transform.TransformPoint(new Vector3(0, 0, 0));
                            pos.x = scanX;
                            pos.y += (hit.point.y - transform.position.y) / 2f;
                            pos.z = scanZ;

                            toSpawn = voiceClass.AddBarrierAtPoint(0, pos, Quaternion.identity, scale);
                        }
                        GameObject[] selectableObjs = GameObject.FindGameObjectsWithTag("Barrier");
                        for (int j = 0; j < selectableObjs.Length; j++)
                        {
                            if (selectableObjs[j].name.Contains("Object"))
                            {
                                if (toSpawn != null && selectableObjs[j].GetComponent<BoxCollider>().bounds.Intersects(toSpawn.gameObject.GetComponent<BoxCollider>().bounds))
                                {
                                    Destroy(toSpawn.gameObject);
                                    toSpawn = null;
                                    break;
                                }
                            }
                        }
                        GameObject[] robotObjs = GameObject.FindGameObjectsWithTag("RobotCollider");
                        for (int j = 0; j < robotObjs.Length; j++)
                        {
                            if (toSpawn != null && toSpawn.gameObject.GetComponent<BoxCollider>().bounds.Intersects(robotObjs[j].GetComponent<BoxCollider>().bounds))
                            {
                                Destroy(toSpawn.gameObject);
                                toSpawn = null;
                                break;
                            }
                        }
                        if (iz > 0 && toSpawn != null && prevBarrier != null)
                        {
                            if (toSpawn.gameObject.GetComponent<BoxCollider>().bounds.Intersects(prevBarrier.gameObject.GetComponent<BoxCollider>().bounds))
                            {

                                Vector3 scale0 = prevBarrier.transform.localScale;
                                Vector3 scale1 = toSpawn.transform.localScale;

                                float largestYScale = (scale0.y > scale1.y) ? scale0.y : scale1.y;
                                float shiftY = (scale0.y > scale1.y) ? 0 : (scale1.y - scale0.y) / 2f;

                                prevBarrier.transform.position += new Vector3(0, shiftY, scanDelta / 2);
                                prevBarrier.transform.localScale = new Vector3(scale0.x, largestYScale, scale0.z + scanDelta);
                                Destroy(toSpawn.gameObject);
                                toSpawn = null;
                            }
                        }
                    }
                }
                if (workplaceBarriers[ix, iz] != null) Destroy(workplaceBarriers[ix, iz].gameObject);
                if (toSpawn != null) prevBarrier = toSpawn;
                workplaceBarriers[ix, iz] = toSpawn;
                if (checkX) yield return null;
            }
        }
        refreshing = false;
        firstScan = false;
    }
}
