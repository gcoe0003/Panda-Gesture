﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


namespace RosSharp.RosBridgeClient
{
    class PickRequest : UnityPublisher<MessageTypes.Moveit.CollisionObject>
    {
        private List<MessageTypes.Moveit.CollisionObject> message_queue;
        protected override void Start()
        {
            base.Start();
            InitialiseMessage();
        }
        private void InitialiseMessage()
        {
            message_queue = new List<MessageTypes.Moveit.CollisionObject>();
        }
        private void Update()
        {
            // If there is message in the queue
            if (message_queue.Count > 0)
            {
                // Publish the first message from queue. Also check if null before publishing (this cause null ref error at the start)
                this?.Publish(message_queue[0]);
                // Then remove it
                message_queue.RemoveAt(0);
            }
        }
        // Update PlanningScene on ROS planner
        public void UpdateScene(MessageTypes.Moveit.CollisionObject new_obj)
        {
            Debug.Log("Picker");
            // Check if null before adding. (This is called from SceneWatcher when init, get null object ref error at the start)
            message_queue?.Add(new_obj);
        }
    }
}