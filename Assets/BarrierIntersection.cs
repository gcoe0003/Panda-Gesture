﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierIntersection : MonoBehaviour
{
    public GameObject intersectingObj;
    private void OnTriggerEnter(Collider col)
    {
        intersectingObj = col.gameObject;
    }
}
