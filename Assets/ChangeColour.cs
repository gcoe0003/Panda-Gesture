﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChangeColour : MonoBehaviour
{
    public void Change()
    {
        if (transform.GetComponent<TextMeshPro>().color == Color.red) transform.GetComponent<TextMeshPro>().color = Color.blue;
        else transform.GetComponent<TextMeshPro>().color = Color.red;
        
    }
}
