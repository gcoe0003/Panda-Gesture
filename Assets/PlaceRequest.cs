﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


namespace RosSharp.RosBridgeClient
{
    class PlaceRequest : UnityPublisher<MessageTypes.Geometry.Vector3>
    {
        private List<MessageTypes.Geometry.Vector3> message_queue;
        protected override void Start()
        {
            base.Start();
            InitialiseMessage();
        }
        private void InitialiseMessage()
        {
            message_queue = new List<MessageTypes.Geometry.Vector3>();
        }
        private void Update()
        {
            // If there is message in the queue
            if (message_queue.Count > 0)
            {
                // Publish the first message from queue. Also check if null before publishing (this cause null ref error at the start)
                this?.Publish(message_queue[0]);
                // Then remove it
                message_queue.RemoveAt(0);
            }
        }
        // Update PlanningScene on ROS planner
        public void UpdateScene(MessageTypes.Geometry.Vector3 new_point)
        {
            Debug.Log("Placer");
            // Check if null before adding. (This is called from SceneWatcher when init, get null object ref error at the start)
            message_queue?.Add(new_point);
        }
    }
}