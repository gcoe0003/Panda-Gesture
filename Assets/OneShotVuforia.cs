﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneShotVuforia : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject target;
    public GameObject robot;

    public void Found() 
    {
        StartCoroutine(TestStability()); // when Vuforia has found an object it runs a stability check, if all object stable vuforia can be disabled
    }
    public IEnumerator TestStability()
    {
        bool unstable = (transform.parent != null);
        if (unstable)
        {
            transform.parent.position = transform.parent.position + Vector3.up * 0.1f; // Apply small displacement so that we can detect when the object is actively tracked
            Vector3 tempPos = transform.position;
            bool hasMoved = true;
            yield return new WaitForSeconds(4.0f);
            while (unstable)
            {
                while (hasMoved) // detect if actively tracked from correcting displacement shift
                {
                    yield return new WaitForSeconds(2.0f);
                    hasMoved = (Vector3.Distance(transform.position, tempPos) < 0.01f);

                    tempPos = transform.position;
                }
                yield return new WaitForSeconds(2.0f);
                unstable = (Vector3.Distance(transform.position, tempPos) > 0.01f) && (Vector3.Distance(transform.position, robot.transform.position) < 2.0f);
                // check that the corrected position has stabilised and is near robot
                Debug.Log(Vector3.Distance(transform.position, robot.transform.position));

                tempPos = transform.position;
            }
        }
        transform.parent = target.transform.parent;
        target.SetActive(false);

        yield return new WaitForSeconds(1.0f); // small pause to allow change to be implemented (idk unity is weird/my code is bad)

        // Are all objects found and stable? if so vuforia can go to sleep
        if (GameObject.FindGameObjectsWithTag("Vuforia").Length == 0) Vuforia.VuforiaBehaviour.Instance.enabled = false;

    }
    public IEnumerator Track()
    {
        yield return new WaitForSeconds(1.5f);
        Found();
    }
    public void Refresh()
    {
        target.SetActive(true);
        transform.parent = target.transform;
        StartCoroutine(Track());
        Vuforia.VuforiaBehaviour.Instance.enabled = true;
    }
}
