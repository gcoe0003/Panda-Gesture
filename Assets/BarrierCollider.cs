﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierCollider : MonoBehaviour
{
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Checker")
        {
            col.gameObject.GetComponent<LinkLine>().MakeRed();
        }
    }
    private void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Checker")
        {
            col.gameObject.GetComponent<LinkLine>().MakeRed();
        }
    }
}
