﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkLine : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject lineSeg;
    public GameObject pointSeg;
    public GameObject otherObj;

    public void MakeRed()
    {
        if (lineSeg != null) lineSeg.GetComponent<MeshRenderer>().material.color = Color.red;
        if (pointSeg != null) pointSeg.GetComponent<MeshRenderer>().material.color = Color.red;
        Destroy(otherObj);
        Destroy(this.gameObject);
    }
}
